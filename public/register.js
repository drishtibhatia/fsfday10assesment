(function(){

    var Register = angular.module("Register",[]);

    //defining a service 
    var RegisterSvc = function($http){
      var registerSvc = this;

      registerSvc.createUser = function(user) {
			return ($http.get("/signUp", {
				params: user
			}));
		};
  
      registerSvc.getRegistered = function() {
        return ($http.get("/registered")
            .then(function(result) {
                 return (result.data);
				}));
		};

    } 

    //creating services 
    Register.service("RegisterSvc",["$http",RegisterSvc]);
})();