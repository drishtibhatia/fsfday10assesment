(function() {
	var RegisterApp = angular.module("RegisterApp", ["Register"]);

    //intailizing values on form 
	var initForm = function(ctrl) {
		ctrl.email = "";
		ctrl.password = "";
		ctrl.username = "";
		ctrl.gender = "";
		ctrl.dateOfBirth = "";
		ctrl.address = "";
        ctrl.country = "";
        ctrl.contact = "";
	}


    //create new user 
    var createUserObject = function(ctrl) {
        
		return ({
			email: ctrl.email,
			password: ctrl.password,
			username: ctrl.username,
			gender: ctrl.gender,
			dateOfBirth: ctrl.dateOfBirth,
			address: ctrl.address,
            country: ctrl.country,
            contact: ctrl.contact,
		});
	}



    //controller function()
    var RegisterCtrl = function(RegisterSvc) {
		var registerCtrl = this;


		initForm(registerCtrl);

		registerCtrl.registerUser = function(){

			RegisterSvc.createUser(createUserObject(registerCtrl))
			.then(function(){
                registerCtrl.status = "Registered Successfully";
				initForm(registerCtrl);
			})
            .catch(function(status){
                registerCtrl.status = "Error in registering.";
        });
		}
    }
	RegisterApp.controller("RegisterCtrl", [ "RegisterSvc", RegisterCtrl ]);
})();