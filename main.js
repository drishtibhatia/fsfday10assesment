//Load the libraries
var path = require("path");
var express = require("express");

//Create an instance of the application
var app = express();

//register new user to gobal array 
var register = [];

//createNewUser on submit 
var createNewUser = function(user) {
	return ({
		email: user.email,
        password:user.password,
        name:user.name,
        gender:user.gender,
        dateOfBirth:user.dateOfBirth,
        adress:user.address,
        country:user.country,
        contact:user.contact,
	});
}

app.use("/libs", express.static(path.join(__dirname, "bower_components")));
app.use(express.static(path.join(__dirname, "public")));

//The parameters are going to be in a query string
app.get("/signUp", function(req, resp) {
	register.push(createNewUser(req.query));
	console.log("New User \n %s", JSON.stringify(register));
	resp.status(201).end();
});

//to get the user from array 
app.get("/registered", function(req, resp) {
	resp.status(200);
	resp.type("application/json");
	resp.json(register);
});

    //middle ware to handle error if dirname is not found
app.use(function(req,resp){
    resp.status(404);
    resp.type("text/html"); 
    resp.send("<h1> File not Found</h1><p> The Current time is "+ new Date() + "</p>");
});

//Setup the server
app.set("port", process.env.APP_PORT || 3000);

app.listen(app.get("port"), function() {
	console.log("Application started at %s on port %d"
			, new Date(), app.get("port"));
});